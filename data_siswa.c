#include <stdio.h>

int main() {
  // Deklarasikan variabel
  char nama[100];
  char NIM[100];
  char kelas[100];

  // Masukkan nama
  printf("Masukkan nama: ");
  scanf("%s", nama);

  // Masukkan NIM
  printf("Masukkan NIM: ");
  scanf("%s", NIM);

  // Masukkan kelas
  printf("Masukkan kelas: ");
  scanf("%s", kelas);

  // Cetak nama
  printf("Nama: %s\n", nama);

  // Cetak NIM
  printf("NIM: %s\n", NIM);

  // Cetak kelas
  printf("Kelas: %s\n", kelas);

  return 0;
}
