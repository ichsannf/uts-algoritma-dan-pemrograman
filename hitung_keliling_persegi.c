#include <stdio.h>

int main() {
  // Deklarasikan variabel
  float sisi;
  float luas;
  float keliling;

  // Masukkan sisi
  printf("Masukkan sisi: ");
  scanf("%f", &sisi);

  // Hitung luas
  luas = sisi * sisi;

  // Hitung keliling
  keliling = 4 * sisi;

  // Cetak luas
  printf("Luas persegi: %.2f\n", luas);

  // Cetak keliling
  printf("Keliling persegi: %.2f\n", keliling);

  return 0;
}
